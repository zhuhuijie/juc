package com.zhj.juc.demo5;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @PackageName:com.zhj.juc.demo5
 * @ClassName:Test1Callable
 * @auter: 朱慧杰
 * @date:2020/7/23 0023 21:33
 **/
public class Test1Callable {

    public static void main(String[] args) {

        // new Thread(new FutureTask<Integer>(new MyThread())).start();

        new Thread().start();

        MyThread thread = new MyThread();
        // 适配类
        FutureTask<Integer> task = new FutureTask<>(thread);

        new  Thread(task,"A").start();

        new  Thread(task,"B").start(); // 结果会被缓存 可能会被阻碍

        Integer num = null;

        try {
            num = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println(num);


    }

}

class MyThread implements Callable <Integer> {

    @Override
    public Integer call() {
        System.out.println("call()启动！");
        return 1024;
    }

}
