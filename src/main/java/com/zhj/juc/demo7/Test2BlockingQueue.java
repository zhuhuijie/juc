package com.zhj.juc.demo7;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @PackageName:com.zhj.juc.demo7
 * @ClassName:Test2BlockingQueue
 * @auter: 朱慧杰
 * @date:2020/7/24 0024 9:18
 **/
public class Test2BlockingQueue {

    public static void main(String[] args) throws Exception {
        //put ,take 会阻塞等待
        test1();
        test2();
    }

    public static void test1() throws Exception {
        // 必须填队列大小
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue(3);


        System.out.println("===============================");

        System.out.println(blockingQueue.remove());

        System.out.println(blockingQueue.element());

        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());

        // System.out.println(blockingQueue.remove()); 会抛异常

        System.out.println(blockingQueue.poll(2,TimeUnit.SECONDS)); // 返回null,不会抛出异常

    }

    public static void test2() throws Exception {
        // 必须填队列大小
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue(3);

        blockingQueue.put("a");
        blockingQueue.put("b");
        blockingQueue.put("c");
        // blockingQueue.put("d");
        System.out.println("===============================");

        System.out.println(blockingQueue.take());

    }
}
