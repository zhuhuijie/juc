package com.zhj.juc.demo9;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @PackageName:com.zhj.juc.demo9
 * @ClassName:Test1
 * @auter: 朱慧杰
 * @date:2020/7/24 0024 17:05
 **/
public class Test1 {

    /**
     * 函数式接口，只有一个方法 简化编程模型，新版本框架大量应用 例：Runnable foreach的参数（消费者类型的函数式接口）
     * Function<T,R></T,R> T 传入值 ， R返回值
     * @param args
     */
    public static void main(String[] args) {
        Function function = new Function<String,String>() {
            @Override
            public String apply(String str) {
                return str;
            }
        };

        Function say = (str)->{
            return str;
        };
        System.out.println(((Function<String, String>) function).apply("你好啊！"));

        System.out.println(say.apply("123"));

        // 断定性接口 返回值为boolean
        Predicate<String> predicate = (str)->{return str.isEmpty();};
        System.out.println(predicate.test("error"));
    }

}
