package com.zhj.juc.demo9;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @PackageName:com.zhj.juc.demo9
 * @ClassName:Test2
 * @auter: 朱慧杰
 * @date:2020/7/24 0024 17:20
 **/
public class Test2 {

    public static void main(String[] args) {
        // 消费型接口 只有输入没有返回值
        Consumer<String> consumer = str->{
            System.out.println(str);
        };
        consumer.accept("您好！");
        // 生产型接口 只有返回没有输入
        Supplier supplier = ()->{return "生产了一个小产品";};
        System.out.println(supplier.get());
    }

}
