package com.zhj.juc.demo9;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @PackageName:com.zhj.juc.demo9
 * @ClassName:User
 * @auter: 朱慧杰
 * @date:2020/7/24 0024 17:31
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private Integer id;
    private String name;
    private Integer age;

}
