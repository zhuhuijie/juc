package com.zhj.juc.demo9;

import java.util.Arrays;
import java.util.List;

/**
 * @PackageName:com.zhj.juc.demo9
 * @ClassName:Test3Stream
 * @auter: 朱慧杰
 * @date:2020/7/24 0024 17:29
 **/
public class Test3Stream {

    public static void main(String[] args) {

        User u1 = new User(1,"a",8);
        User u2 = new User(2,"b",38);
        User u3 = new User(3,"c",28);
        User u4 = new User(4,"d",22);
        User u5 = new User(5,"e",24);

        List<User> list = Arrays.asList(u1,u2,u3,u4,u5);

        // lambda 表达式 、链式编程、函数式接口、Stream流式计算
        // 计算交给stream
        list.stream()
                .filter(u->{return u.getId()%2!=0;})
                .filter(u->{return u.getAge()>=23;})
                .map(u->{return u.getName().toUpperCase();})
                .sorted((uu1,uu2)->{return uu2.compareTo(uu1);})
                .limit(1)
                .forEach(System.out::println);
    }

}
