package com.zhj.juc.demo11;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @PackageName:com.zhj.juc.demo11
 * @ClassName:Test1
 * @auter: 朱慧杰
 * @date:2020/7/24 0024 21:26
 **/
public class Test1 {

    public static void main(String[] args) throws Exception {
        // 发起一个请求
        // 没有返回值的异步回调
        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(()->{
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " runAsync => void ");
        });

        System.out.println("hello");

        completableFuture.get();

        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(()->{
            int i = 10/0;
            return 1024;
        });

        System.out.println(future.whenComplete((t, u) -> {
            System.out.println(t + " + " + u);
        }).exceptionally((e) -> {
            e.printStackTrace();
            return 404;
        }).get());
    }

}
