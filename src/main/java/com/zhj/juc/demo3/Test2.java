package com.zhj.juc.demo3;

import java.util.concurrent.TimeUnit;

/**
 * @PackageName:com.zhj.juc.demo3
 * @ClassName:Test2
 * @auter: 朱慧杰
 * @date:2020/7/23 0023 10:19
 **/
public class Test2 {

    public static void main(String[] args) {
        Phone2 phone = new Phone2();
        Phone2 phoneSE = new Phone2();

        new Thread(()->{
            phone.sendSms();
        },"A").start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(()->{
            phoneSE.call();
        },"B").start();

        new Thread(()->{
            phone.hello();
        },"C").start();

    }

}

class Phone2 {

    // synchronized 锁 锁的对象是方法的调用者，谁先拿到谁先执行
    // static 静态方法 类一加载就有了，锁的是Class
    public static synchronized void sendSms() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " 发短信");
    }

    // 锁的调用者

    public synchronized void call() {
        System.out.println(Thread.currentThread().getName() + " 打电话");
    }

    public void hello() {
        System.out.println(Thread.currentThread().getName() + " hello");
    }

}