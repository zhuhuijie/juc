package com.zhj.juc.demo1;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.FutureTask;

/**
 * @PackageName:com.zhj.juc.demo1
 * @ClassName:Test1
 * @auter: 朱慧杰
 * @date:2020/7/22 0022 21:36
 **/
public class Test1 extends Thread {

    // 线程的几种状态 ：
    // 新生 NEW -》
    // 运行 RUNNABLE -》
    // 阻塞 BLOCKED -》
    // 等待 WAITING -》
    // 超时等待 TIME_WAITING -》
    // 终止 TERMINATED
    public static void main(String[] args) {

        System.out.println("计算机核心：" + Runtime.getRuntime().availableProcessors());

        new Test1().start();

        new Thread(new Test01()).start();

        Test001 callable = new Test001();
        FutureTask<Integer> task = new FutureTask<>(callable);
        new Thread(task).start();

    }

    @Override
    public void run() {
        System.out.println("我是线程的第一种种创建方式！-----继承Thread类");
        super.run();
    }
}

class Test01 implements Runnable {

    @Override
    public void run() {
        System.out.println("我是线程的第二种创建方式！-----实现Runnable接口");
    }
}

class Test001 implements Callable<Integer> {

    // 可以有返回值 可以抛出异常
    @Override
    public Integer call() throws Exception {

        System.out.println("我是线程的第三种创建方式！----实现Callable接口");

        return null;
    }
}
