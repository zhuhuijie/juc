package com.zhj.juc.demo15;

import java.util.concurrent.TimeUnit;

/**
 * @PackageName:com.zhj.juc.demo15
 * @ClassName:DeaadLock
 * @auter: 朱慧杰
 * @date:2020/7/25 0025 19:51
 **/
public class DeaadLock {

    // 死锁排查  jps -l   查看进程号  jstack 11172  查看堆栈信息

    public static void main(String[] args) {
        String lockA = "锁A";
        String lockB = "锁B";
        new Thread(new MyThread(lockA,lockB),"T1").start();
        new Thread(new MyThread(lockB,lockA),"T2").start();
    }

}

class MyThread implements Runnable
{

    private String lockA;
    private String lockB;

    public MyThread(String lockA, String lockB) {
        this.lockA = lockA;
        this.lockB = lockB;
    }

    @Override
    public void run() {
        synchronized (lockA) {
            System.out.println(Thread.currentThread().getName() + " lock : " + lockA + " to get : " + lockB);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lockB) {
                System.out.println(Thread.currentThread().getName() + " lock : " + lockB + " to get : " + lockA);
            }
        }
    }
}
