package com.zhj.juc.demo8;

import java.util.concurrent.*;

/**
 * @PackageName:com.zhj.juc.demo8
 * @ClassName:Test2ThreadPool
 * @auter: 朱慧杰
 * @date:2020/7/24 0024 16:28
 **/
public class Test2ThreadPool {

    public static void main(String[] args) {

        System.out.println(Runtime.getRuntime().availableProcessors());
        /**
         * 手动创建线程 （其他方式不安全，工作中用手动创建的线程池）
         * 七个参数：
         * 1. 核心线程数
         * 2. 最大线程数  本机核心数Runtime.getRuntime().availableProcessors() 1. cpu密集型 几核就是几  2. io密集型 判断你系统十分耗Io的  15个大型任务线程数要大于15 2 倍
         * 3. 等待时间
         * 4. 时间单位
         * 5. 队列 等候区
         * 6. 创建线程的工厂
         * 7. 拒绝策略 ： 4种
         * AbortPolicy 队列满进入会抛出异常
         * CallerRunsPolicy 哪来的回哪
         * DiscardPolicy 满了不会抛出异常，任务也不会执行
         * DiscardOldestPolicy 满了会尝试和第一个人竞争，失败也不会抛出异常
         */
        ExecutorService service = new ThreadPoolExecutor(
                2,
                5,
                3,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.DiscardOldestPolicy()
        );

        try {
            for (int i = 0; i < 9; i++) {
                service.execute(()->{
                    System.out.println(Thread.currentThread().getName() + "处理业务！");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            service.shutdown();
        }

    }

}
