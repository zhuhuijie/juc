package com.zhj.juc.demo8;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @PackageName:com.zhj.juc.demo8
 * @ClassName:Test1ThreadPool
 * @auter: 朱慧杰
 * @date:2020/7/24 0024 15:57
 **/
public class Test1ThreadPool {

    /**
     * 线程池：
     * 好处：
     * 1. 降低资源消耗
     * 2. 提高响应速度
     * 3. 方便管理
     * 复用，控制最高并发
     * @param args
     */
    public static void main(String[] args) {
        ExecutorService service1 = Executors.newSingleThreadExecutor();          // 单个线程
        ExecutorService service2 = Executors.newFixedThreadPool(5);   // 固定线程的个数
        ExecutorService service3 = Executors.newCachedThreadPool();              // 可伸缩的

        try {
            for (int i = 0; i < 100; i++) {
                service3.execute(()->{
                    System.out.println(Thread.currentThread().getName() + " ok");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            service1.shutdown();
            service2.shutdown();
            service3.shutdown();
        }
    }

}
