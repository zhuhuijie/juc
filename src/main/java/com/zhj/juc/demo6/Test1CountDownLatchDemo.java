package com.zhj.juc.demo6;

import java.util.concurrent.CountDownLatch;

/**
 * @PackageName:com.zhj.juc.demo6
 * @ClassName:Test1CountDownLatchDemo
 * @auter: 朱慧杰
 * @date:2020/7/23 0023 21:55
 **/
public class Test1CountDownLatchDemo {

    public static void main(String[] args) throws Exception {

        // 总数是6 ，必须执行任务，再使用
        CountDownLatch countDownLatch = new CountDownLatch(6);

        for (int i = 0; i < 6; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + " : Go out!");
                countDownLatch.countDown(); // 数量减一
            }, String.valueOf(i)).start();
        }

        countDownLatch.await(); // 等待归零

        System.out.println("关门！");

    }

}
