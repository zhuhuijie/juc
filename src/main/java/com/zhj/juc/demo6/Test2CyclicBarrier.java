package com.zhj.juc.demo6;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @PackageName:com.zhj.juc.demo6
 * @ClassName:Test2CyclicBarrier
 * @auter: 朱慧杰
 * @date:2020/7/23 0023 22:01
 **/
public class Test2CyclicBarrier {

    public static void main(String[] args) {


        // 召唤龙珠的线程  如果超过7 会一直等程序停不了
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7,()->{
            System.out.println("召唤神龙成功！！！");
        });

        for (int i = 1; i <= 7; i++) {
            final int temp = i;
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + "收集了 " + temp + " 颗龙珠！");

                try {
                    cyclicBarrier.await(); // 等待
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }

    }

}
