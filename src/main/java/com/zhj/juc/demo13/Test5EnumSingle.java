package com.zhj.juc.demo13;

import java.lang.reflect.Constructor;

/**
 * @PackageName:com.zhj.juc.demo13
 * @ClassName:Test5EnumSingle
 * @auter: 朱慧杰
 * @date:2020/7/25 0025 10:03
 **/
public enum  Test5EnumSingle {

    // enum 枚举 本身也是一个类
    INSTANCE;

    public Test5EnumSingle getInstance(){
        return INSTANCE;
    }

}

class Test {

    public static void main(String[] args) throws Exception {
        Test5EnumSingle instance1 = Test5EnumSingle.INSTANCE;

        Constructor<Test5EnumSingle> declaredConstructor = Test5EnumSingle.class.getDeclaredConstructor(String.class,int.class);

        declaredConstructor.setAccessible(true);
        Test5EnumSingle instance2 = declaredConstructor.newInstance();
        System.out.println(instance1);
        System.out.println(instance2);
    }

}
