package com.zhj.juc.demo13;

/**
 * @PackageName:com.zhj.juc.demo13
 * @ClassName:Test4Holder
 * @auter: 朱慧杰
 * @date:2020/7/25 0025 9:44
 **/
public class Test4Holder {

    // 单例必须构造器私有
    private Test4Holder () {

    }


    public static class InnerClass {

    }
}
