package com.zhj.juc.demo13;

/**
 * @PackageName:com.zhj.juc.demo13
 * @ClassName:Test2Hungry
 * @auter: 朱慧杰
 * @date:2020/7/25 0025 9:32
 **/
public class Test2Hungry {
    // 饿汉式单例
    // 可能会浪费空间
    private Byte[] data1 = new Byte[1024*1024];
    private Byte[] data2 = new Byte[1024*1024];
    private Byte[] data3 = new Byte[1024*1024];
    private Byte[] data4 = new Byte[1024*1024];

    private Test2Hungry() {

    }

    private final static Test2Hungry HUNGRY = new Test2Hungry();

    public static Test2Hungry getInstance() {
        return HUNGRY;
    }

}
