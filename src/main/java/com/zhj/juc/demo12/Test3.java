package com.zhj.juc.demo12;

/**
 * @PackageName:com.zhj.juc.demo13
 * @ClassName:Test1
 * @auter: 朱慧杰
 * @date:2020/7/25 0025 9:16
 **/
public class Test3 {

    private volatile int num = 0;
    /**
     * 指令重排
     * 源代码--》编译器优化重排--》指令并行也可能会重排--》内存系统也会重排--》执行
     * 指令重排 会考虑数据之间的依赖性
     * volatile 由于内存屏障 可以避免指令重排带来影响
     * @param args
     */
    public static void main(String[] args) {

    }

}
