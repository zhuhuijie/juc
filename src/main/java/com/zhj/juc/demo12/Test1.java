package com.zhj.juc.demo12;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

/**
 * @PackageName:com.zhj.juc.demo12
 * @ClassName:Test1
 * @auter: 朱慧杰
 * @date:2020/7/24 0024 22:05
 **/
public class Test1 {

    private volatile static int num = 0;
    /**
     * Volatile
     * 1.保证可见性
     * 2.不保证原子性
     * 3.禁止指令重拍
     *
     * JMM : java 内存模型，不存在的东西
     * 1. 线程解锁前，必须把共享变量 立即刷回主存
     * 2. 线程加锁前，必须读取主存的最新值到工作内存中
     * 3. 加锁和解锁是同一把锁
     *
     * * 8种操作 :  成对出现
     * （加锁lock、解锁unlock） 从主存
     * （读read、加载load ） 到 线程内存  然后在线程中
     * （使用use、返回assign）
     * （写入write、存储store） 到主存
     * @param args
     */
    public static void main(String[] args) {
        // 让线程A知道主内存的变化 在需要知道的地方加volatile
        // 原子性 线程A在
        new Thread(()->{
            while (num==0) {

            }
        },"A").start();

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        num = 1;
        System.out.println(num);
    }

}
