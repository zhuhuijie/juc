package com.zhj.juc.demo14;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @PackageName:com.zhj.juc.demo14
 * @ClassName:Test1CAS
 * @auter: 朱慧杰
 * @date:2020/7/25 0025 10:20
 **/
public class Test1CAS {

    // CAS compareAndSet 比较并交换
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(2020);

        // 期望 更新
        // public final boolean compareAndSet(int expect, int update)
        // 如果我希望的值达到了，那么就更新，否则，就不更新 CAS 是CPU的并发源于
        System.out.println(atomicInteger.compareAndSet(2020,2021));
        System.out.println(atomicInteger.get());
        System.out.println(atomicInteger.compareAndSet(2020,2021));
        System.out.println(atomicInteger.get());
    }

}
