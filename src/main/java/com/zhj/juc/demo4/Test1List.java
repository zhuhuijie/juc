package com.zhj.juc.demo4;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @PackageName:com.zhj.juc.demo4
 * @ClassName:Test1
 * @auter: 朱慧杰
 * @date:2020/7/23 0023 20:50
 **/
public class Test1List {

    public static void main(String[] args) {

        // List<String> list = new ArrayList<>();
        // 解决方案
        // 1.  List<String> list = new Vector<>();  // 线程安全
        // 2.  List<String> list = Collections.synchronizedList(new ArrayList<>());
        // 3.  线程安全类  CopyOnWriteArrayList  写入时复制程序设计的优化 多个线程写入会覆盖，写入时避免复制
        // CopyOnWriteArrayList 比 ArrayList 效率高没有用synchronized
        // 读写分离
        List<String> list = new CopyOnWriteArrayList<>();
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,6));
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }

}
