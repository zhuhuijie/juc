package com.zhj.juc.demo4;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @PackageName:com.zhj.juc.demo4
 * @ClassName:Test3Map
 * @auter: 朱慧杰
 * @date:2020/7/23 0023 21:21
 **/
public class Test3Map {

    public static void main(String[] args) {
        // map 是这样用的吗，工作中不用
        // 默认等价 0.75 16
        // Map<String, String> map = new HashMap<>();
        // 1. Map<String, String> map = Collections.synchronizedMap(new HashMap<>());

        Map<String, String> map = new ConcurrentHashMap<>();

        for (int i = 0; i < 30; i++) {
            new Thread(()->{
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0,6));
                System.out.println(map);
            },String.valueOf(i)).start();
        }

    }

}
