package com.zhj.juc.demo4;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @PackageName:com.zhj.juc.demo4
 * @ClassName:Test2Set
 * @auter: 朱慧杰
 * @date:2020/7/23 0023 21:12
 **/
public class Test2Set {

    public static void main(String[] args) {

        // HashSet<Object> set = new HashSet<>();
        // 1.Set<String> set = Collections.synchronizedSet(new HashSet<>());
        // 2.Set<String> set = new CopyOnWriteArraySet<>();

        Set<String> set = new HashSet<>();

        for (int i = 0; i < 30; i++) {
            new Thread(()->{
                set.add(UUID.randomUUID().toString().substring(0,7));
                System.out.println(set);
            }, String.valueOf(i)).start();
        }

    }

}
